const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const router = express.Router();
const cors = require("cors");
router.use(cors());

const User = require("../models/User");

router.post("/login/user", async (req, res) => {
    try {
      const user = await User.findOne({ email: req.body.email });
      // check if the username exist
      if (!user)
        return res.status(200).send({ error: "this email doesn't exist" });
      // if password is correct
      const validpwd = await bcrypt.compare(req.body.password, user.password);
      if (!validpwd) return res.status(200).send({error: "Email or Password is wrong"});
      // Create and assign token
      const token = jwt.sign({ _id: user.id }, process.env.TOKEN_SECRET);
      // res.header("Authorization", "Bearer" + token);
      res.cookie("token", token, {
        maxAge: 30 * 60 * 1000, // 30 minutes
        httpOnly: true,
        //secure:true
      });
      res.send({ token: token, userId: user.id });
      // res.send("login");
    } catch {
      res.status(404);
      res.send({ error: "user doesn't exist!" });
    }
  });

module.exports = router