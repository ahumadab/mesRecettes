const express = require("express");
const router = express.Router();
const cors = require("cors");
const bcrypt = require("bcryptjs");
const validate = require("../middlewares/validate");
const cookieParser = require("cookie-parser");

const User = require("../models/User");

//------ middleware ---------//

router.use(cookieParser());
router.use(cors());

// create new user

router.post("/new", async (req, res) => {
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);
    const user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashPassword,
    })
    await user.save()
    res.send({success: "success user created"});
})

// list user by id

router.get("/profile/id/:id", validate.checkJWT, async (req, res) => {
    try {
      const user = await User.findOne({ _id: req.params.id });
      if (!user) throw Error();
      res.send(user);
    } catch {
      res.status(404);
      res.send({ error: "User doesn't exist!" });
    }
  });

router.get("/profile/email/:email", async (req, res) => {
  try {
    const user = await User.findOne({ email: req.params.email });
    if (!user) throw Error();
    res.send({ success: "User email exists"});
  } catch {
    res.status(200);
    res.send({ error: "User doesn't exist!" });
  }
});

module.exports = router;